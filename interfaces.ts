// HubSpot response typing

interface VersionItem {
  name?: string;
  value?: string;
  timestamp?: number;
  sourceId?: string;
  source?: string;
  sourceVid?: object[];
  sourceMetadata?: string;
}

interface PropertyItem {
  value: string;
  timestamp?: number;
  source?: string;
  sourceId?: string;
  versions?: VersionItem[];
}

interface CompanyProps {
  name: PropertyItem;
  country_region_new_nodotuse?: PropertyItem;
  country?: PropertyItem;
  city?: PropertyItem;
  vat_number?: PropertyItem;
  description?: PropertyItem;
  zip?: PropertyItem;
  website?: PropertyItem;
  address?: PropertyItem;
  address2?: PropertyItem;
  domain?: PropertyItem;
}

interface PersonProps {
  firstname: PropertyItem;
  lastname: PropertyItem;
  email: PropertyItem;
  mobilephone?: PropertyItem;
}

interface DealProps {
  dealname: PropertyItem;
  amount: PropertyItem;
  description: PropertyItem;
  second_responsible: PropertyItem;
  hubspot_owner_id: PropertyItem;
  type_of_deal: PropertyItem;
  pipeline: PropertyItem;
}

// -- Exports --

export type DealId = string;
export type ContactId = string;
export type CompanyId = string;
export type PersonId = string;

export interface DealAssociations {
  associatedVids: ContactId[];
  associatedCompanyIds: CompanyId[];
}

export interface DealEvent {
  objectId: DealId;
  objectType?: string;
  objectTypeId?: string;
  properties?: PropertyItem[];
  version?: number;
  secondaryIdentifier?: null;
  isDeleted?: boolean;
}

export interface CrmAssociations {
  results: number[];
}

export interface LineItem {
  properties: {
    quantity: { value: string };
    hs_product_id: { value: string };
  };
}

export interface Product {
  objectId: number;
  properties: {
    price: { value: string };
    name: { value: string };
  };
}

export interface HubDeal {
  dealId: number;
  isDeleted?: boolean;
  typeOfDeal: string;
  associations: DealAssociations;
  properties: DealProps;
}

export interface HubCompany {
  companyId: CompanyId;
  isDeleted?: boolean;
  properties: CompanyProps;
}

export interface HubOwner {
  firstName: string;
  lastName: string;
  email: string;
}

export interface HubPerson {
  vid: number;
  properties: PersonProps;
}

export interface HubBatchUpdate {
  objectId: number;
  properties: {
    name: string;
    value: string;
  };
}

export interface HubPipelines {
  results: {
    pipelineId: string;
    label: string;
  }[];
}
