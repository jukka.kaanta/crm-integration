// import { common stuff } from 'common-stuff'
import config from "./config";

const s3bucket = new exportedAWS.S3();

export const uploadToS3 = async (key: string, body: object) => {
  const params = {
    Bucket: config.BUCKET_NAME,
    Key: `customer-information/${key}.json`,
    Body: JSON.stringify(body)
  };
  return new Promise((resolve, reject) => {
    s3bucket.putObject(params, (err, data) => {
      if (err) {
        log.error(`Bucket upload error: `, { err });
        return reject(err);
      }
      return resolve(data);
    });
  });
};
