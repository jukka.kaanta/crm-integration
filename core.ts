import config from './config';
import { uploadToS3 } from './bucket';
import * as hubspot from './hubspot-connection';
import { DealEvent, HubOwner } from './interfaces';
// import { common stuff } from 'common stuff';

const dealType = (products: LineItem[], pipeline: string): Deal => ({
  startDate: new Date().toISOstring(),
  products: products,
  pipeline: pipeline,
});

// Convert HubSpot contact to a contact person
const ownerToPerson = (owner: HubOwner): Person => ({
  firstName: owner.firstName,
  lastName: owner.lastName,
  email: owner.email,
});

// Create data for Customers microservice
const toSendData = (deal: Deal): customerEvent => ({
  eventType: '/customer-available',
  name: deal.company.name,
  companyId: deal.company.companyId,
  businessId: deal.company.businessId,
  industry: '',
  streetAddress:
    deal.company.address?.addressLine1 ?? '' +
    '\n' +
    deal.company.address?.addressLine2 ?? '',
  postalCode: deal.company.address.postcode,
  city: deal.company.address.city,
  country: deal.company.address.countryCode,
  salesPerson: deal.salesPerson,
  projectManager: deal.salesPerson,
  contactPersons: deal.contacts,
  deal: dealType(
    deal.products.map(p => ({ ...p, suiteId: p.productId })),
    deal.pipelineName
  ),
});

// Check if the deal has needed fields, otherwise create a ticket to HubSpot's
// ticketing system (it's more convenient to browse tickets than logs from
// AWS CloudWatch - from salesman's point of view)
const dealValid = async (evt: DealEvent, deal: Deal) => {
  let msg = '';
  let itemQuantity = 0;

  const ticket = [
    { name: 'subject', value: `Deal ${evt.objectId} enrollment error!` },
    { name: 'hs_pipeline', value: '0' }, // support ticket pipeline
    { name: 'hs_pipeline_stage', value: '1' },
  ];

  deal.products.forEach(product => (itemQuantity += product.amount));
  if (itemQuantity === 0) msg += 'Total product quantity is less than 1\n';
  if (details.businessId === '') msg += 'Missing business ID (VAT)\n';
  if (deal.company.address.addressLine1 === '')
    msg += 'Missing street address\n';
  if (deal.company.address.city === '') msg += 'Missing city\n';
  if (details.country === '') msg += 'Missing country\n';

  if (msg !== '') {
    ticket.push({
      name: 'content',
      value:
        `Deal ${evt.objectId} on pipeline "${deal.pipelineName}" ` +
        `failed enrollment because:\n\n${msg}`,
    });
    log.debug('Deal missing properties, sending a ticket', ticket);
    await hubspot.createTicket(ticket);
    return false;
  } else {
    return true;
  }
};

const uploadDataToS3Bucket = async (
  deal: Deal,
  dealEvent: DealEvent
) => {
  const sendData = toSendData(deal);

  log.debug(`Saving new customer ${deal.company.companyId} to bucket`, {
    deal,
  });
  await uploadToS3(
    `${deal.company.companyId}-${dealEvent.objectId}`,
    sendData
  );
};

// Collect needed data from HubSpot, send to Customers microservice
// and save save the data into S3 Bucket
export const sendToBucket = async (dealEvent: DealEvent) => {
  const deal = await hubspotCore.getDeal(dealEvent);

  if (!(await dealValid(dealEvent, deal))) {
    log.error('Deal not valid because of missing fields', dealEvent);
    return { error: 'Deal not valid, check HubSpot tickets' };
  }

  await updateDealProperties(deal);
  await netsuiteCore.createSalesOrder(deal);
  await uploadDataToS3Bucket(deal, dealEvent);
  return toSendData(deal);
};
