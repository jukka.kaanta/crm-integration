import config from "./config";
import { SNSMessageAttributes } from "aws-lambda";

// import { common stuff } from 'common-stuff'

import { DealEvent } from "./interfaces";
import { sendToBucket } from "./core";

const getDealEvent = (evt: APIEventParsed) => {
  return evt.bodyParsed as DealEvent;
};

// Get deal JSON from HubSpot webhook when a deal status is "closed won"
// and we're only interested in field "objectId"
const getDealReq = {
  type: "object",
  required: ["bodyParsed", "headers"],
  properties: {
    bodyParsed: {
      type: "object",
      required: ["objectId"]
    }
  }
};

// No-one really cares if this service returns JSON data unless an error occurs
const dealResp = {
  type: "object",
  required: ["statusCode"],
  properties: {
    statusCode: { type: "number" }
  }
};

export const dealHandle = makeGwHandler<
  DealEvent,
  customrIntegrationEvent | object
>({
  allowedOrigins: [""],
  allowedRoles: [""],
  eventSchema: getDealReq,
  pickLogicInputFn: getDealEvent,
  logicFn: evt => sendToBucket(evt as DealEvent),
  responseSchema: dealResp
});
