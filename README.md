### CRM-Integration

# The basics

- At HubSpot, a webhook is triggered whenever a deal reaches the "closed won" status
- CRM integration service receives JSON data from the webhook
- All customer data concerning the deal is fetched, validated and saved into AWS S3 bucket