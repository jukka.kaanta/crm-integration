// import { common stuff } from 'common-stuff'

// Throws error if any of given environment variables is not set.
requireEnv([
  "BUCKET_NAME",
  "SERVICE_NAME",
  "TOPIC_ARN",
  "ERROR_TOPIC_ARN",
  "HUBSPOT_API_URL",
  "HUBSPOT_APIKEY",
  "HUBSPOT_APP_SECRET"
]);

const config = {
  ...defaultConfig,
  SERVICE_NAME: process.env.SERVICE_NAME || "",
  TOPIC_ARN: process.env.TOPIC_ARN || "",
  ERROR_TOPIC_ARN: process.env.ERROR_TOPIC_ARN || "",
  HUBSPOT_API_URL: process.env.HUBSPOT_API_URL || "",
  HUBSPOT_APIKEY: process.env.HUBSPOT_APIKEY || "",
  HUBSPOT_APP_SECRET: process.env.HUBSPOT_APP_SECRET || "",
  BUCKET_NAME: process.env.BUCKET_NAME || "",
  ALLOWED_ORIGINS: [""]
};

export default config;
