import axios from "axios";
import qs from "querystring";
import config from "./config";
//  import { ..common stuff }  from 'common stuff'
import {
  CompanyId,
  ContactId,
  CrmAssociations,
  DealId,
  HubCompany,
  HubDeal,
  HubOwner,
  HubPerson,
  HubPipelines,
  LineItem,
  PersonId,
  Product
} from "./interfaces";

// Get needed info from a person
const personInfo = (person: HubPerson): Person => ({
  firstName: person.properties.firstname?.value ?? "",
  lastName: person.properties.lastname?.value ?? "",
  email: (person.properties.email?.value ?? "",
  phone: person.properties.mobilephone?.value ?? ""
});

const hubApiAxiosClient = axios.create();

const hubApi = {
  request: <T>(method, path, data?: object, params?: object): Promise<T> =>
    new Promise((resolve, reject) => {
      hubApiAxiosClient({
        method: method,
        url: config.HUBSPOT_API_URL + path,
        params: { hapikey: config.HUBSPOT_APIKEY, ...params },
        paramsSerializer: params => qs.stringify(params),
        data: data
      })
        .then(resp => {
          log.debug("Response from Hubspot", {
            status: resp.status,
            data: resp.data || undefined
          });
          return resolve(resp.data as T);
        })
        .catch(err => {
          log.error("Hubspot API call failed: " + err, { err });
          return reject(err);
        });
    }),
  get: <T>(path: string, params?: object) =>
    hubApi.request<T>("get", path, null, params),
  put: <T>(path: string, data: object) => hubApi.request<T>("put", path, data),
  post: <T>(path: string, data: object) => hubApi.request<T>("post", path, data)
};

export const getDealById = async (dealId: DealId): Promise<HubDeal> => {
  log.debug("Get deal by id", { dealId });
  return await hubApi.get<HubDeal>(`deals/v1/deal/${dealId}`);
};

export const getCompanyById = async (
  companyId: CompanyId
): Promise<HubCompany> => {
  log.debug("Get company by id", { companyId });
  return (await hubApi.get<HubCompany>(
    `companies/v2/companies/${companyId}`
  )) as HubCompany;
};

// Get a person/contact (vid)
export const getPerson = async (vid: PersonId): Promise<HubPerson> => {
  log.debug("Get person", { personid: vid });
  return (await hubApi.get<HubPerson>(
    `contacts/v1/contact/vid/${vid}/profile`
  )) as HubPerson;
};

export const getOwner = async (ownerId: PersonId): Promise<HubOwner> => {
  log.debug("Get owner", { ownerId });
  return (await hubApi.get<HubOwner>(
    `owners/v2/owners/${ownerId}`
  )) as HubOwner;
};

export const getPipelines = async (): Promise<HubPipelines> => {
  log.debug("Get pipelines");
  return (await hubApi.get<HubPipelines>(
    "crm-pipelines/v1/pipelines/deals"
  )) as HubPipelines;
};

// Get products from the deal. Yep, it has to be this hard:
// 1. get associations, 2. get line items and finally 3. get products
// ..all three separately from the API.
export const getCrmAssociationsByDealId = async (
  dealId: DealId
): Promise<CrmAssociations> => {
  log.debug("Get CRM associations", { dealId });
  return await hubApi.get<CrmAssociations>(
    `crm-associations/v1/associations/${dealId}/HUBSPOT_DEFINED/19`
  );
};

export const getLineItemsByAssociations = async (
  assocs: CrmAssociations
): Promise<LineItem[]> => {
  log.debug("Get line items by associations", { assocs });
  return await Promise.all(
    assocs.results.map(assocId =>
      hubApi.get<LineItem>(`crm-objects/v1/objects/line_items/${assocId}`, {
        properties: "quantity"
      })
    )
  );
};

export const getProductByLineItem = async (
  lineItem: LineItem
): Promise<ProductLineItem> => {
  log.debug("Get product by line item", { lineItem });
  const product = (await hubApi.get(
    `crm-objects/v1/objects/products/${lineItem.properties.hs_product_id.value}`,
    { properties: ["name", "price", "hs_sku"] }
  )) as Product;
  return {
    productName: product.properties.name.value,
    productId: product.objectId.toString(),
    amount: Number(lineItem.properties.quantity.value)
  };
};

export const getProductsByDealId = async (
  dealId: DealId
): Promise<ProductLineItem[]> => {
  log.debug("Get products by dealId", { dealId });
  const associations = await getCrmAssociationsByDealId(dealId);
  const lineItems = await getLineItemsByAssociations(associations);
  return await Promise.all(lineItems.map(getProductByLineItem));
};

export const getContacts = async (
  contactIds: ContactId[]
): Promise<Person[]> => {
  log.debug("Get contacts", { contactIds });
  const promises = [];
  const contacts = [];
  contactIds.forEach(vid => promises.push(getPerson(vid)));
  await Promise.all(promises).then(data => {
    data.forEach(person => contacts.push(personInfo(person)));
  });
  return contacts;
};

export const createProducts = async (products: object[]) => {
  await hubApi.post("crm-objects/v1/objects/products/batch-create", products);
};

export const createTicket = async (ticket: object) => {
  await hubApi.post("crm-objects/v1/objects/tickets", ticket);
};

export const updateDeal = async (dealId: number, properties: object) => {
  return await hubApi.put(`deals/v1/deal/${dealId.toString()}`, properties);
};
